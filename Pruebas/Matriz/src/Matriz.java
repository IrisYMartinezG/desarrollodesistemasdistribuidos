import java.net.ServerSocket;

public class Matriz {
    static Object lock =new Object();

    //Matrices
    static int N = 1000;
    static double[][] A;
    static double[][] B;
    static double[][] C;


    static int i = 0, j = 0, k=0;

    static class Worker extends Thread{
        /*Socket conexion;
        Worker(Socket conexion){
            this.conexion = conexion;
        }
        public void run(){
            try {
                DataInputStream entrada = new DataInputStream(conexion.getInputStream());
                DataOutputStream salida = new DataOutputStream(conexion.getOutputStream());

                double x = 0.0; x = entrada.readDouble();
                System.out.println("Suma recibida por: " + x);

                synchronized (lock) {
                    pi = x + pi;
                }

                entrada.close();
                salida.close();

                conexion.close();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }*/
    }

    public static void main(String[] args) throws Exception{
       if (args.length != 1) {
            System.err.println("<<<<<<<  Uso: Ejecutando  >>>>>>");
            System.err.println("java PI <nodo>");
            System.exit(0);
       }

       int nodo = Integer.valueOf(args[0]);

       if (nodo == 0){
           //Creamos Matrices
           A = new double[N][N];
           B = new double[N][N];
           C = new double[N][N];

           // 1.- Inicializando las matrices
           for (i = 0; i < N; i++){
               for (j = 0; j < N; j++){
                   A[i][j] = ( 3 * i ) + ( 2 * j );
                   B[i][j] = ( 3 * i ) - ( 2 * j );
                   C[i][j] = 0;

               }
           }
           // transpone la matriz B, la matriz traspuesta queda en B
           for (i = 0; i < N; i++){
               for (j = 0; j < i; j++){
                   double t = B[i][j];
                   B[i][j] = B[j][i];
                   B[j][i] = t;
               }
           }
           if (N == 4){
               // imprimiendo A
               System.out.println("\nMatriz A: ");
               //imprimirMatriz(A,N,N);

               // imprimiendo la matriz B
               System.out.println("\nMatriz B 'Transpuesta': ");
               //imprimirMatriz(B,N,N);
        }/*else {
            Socket conexion = null;

            for (; ; ) {
                try {
                    conexion = new Socket("localhost", 50000);
                    System.out.println("Conexión establecida");
                    break;
                } catch (Exception e) {
                    System.out.println("Conexión rechaazada");
                    Thread.sleep(100);
                }
            }

            DataInputStream entrada = new DataInputStream(conexion.getInputStream());
            DataOutputStream salida = new DataOutputStream(conexion.getOutputStream());


            double suma = 0.0;

            int i = 0;

            while (i != 10000000) {
                suma = 4.0 / ((8 * i) + (2 * (nodo - 2)) + 3) + suma;
                i++;
            }

            suma = (nodo % 2) == 0 ? -suma : suma;

            salida.writeDouble(suma);

            entrada.close();
            salida.close();

            conexion.close();*/
        }

    }
