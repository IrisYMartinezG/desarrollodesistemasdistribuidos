/*Autor: Iris Martinez 19 marzo 2021 */


import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.ByteBuffer;

public class P3 {
    static Object lock = new Object();
    
    static int N = 4;

    //declaramos las matrices 
    static int[][] A=new int[N][N];
    static int[][] B=new int[N][N];
    static int[][] C=new int[N][N];

    static class Worker extends Thread{
        Socket conexion;

        Worker(Socket conexion){
            this.conexion = conexion;
        }
        public void run(){
            try {
                DataInputStream entrada = new DataInputStream(conexion.getInputStream());
                DataOutputStream salida = new DataOutputStream(conexion.getOutputStream());

                // Matrices auxiliares A1 y A2, B1, B2
                int[][] A1 = new int[N/2][N];
                int[][] A2 = new int[N/2][N];
                int[][] B1 = new int[N/2][N];
                int[][] B2 = new int[N/2][N];

                int nodo = entrada.readInt();//recibimos el # de nodo

                // Envio las matrices dependiendo del #nodo
                if (nodo == 1) { // Nodo 1

                    ByteBuffer a1 = ByteBuffer.allocate(N*N*4);
                    ByteBuffer b1 = ByteBuffer.allocate(N*N*4);
                    for (int i = 0; i < (N / 2); i++) {
                        for (int j = 0; j < N; j++) {
                            A1[i][j] = A[i][j];
                            a1.putInt(A1[i][j]);
                            B1[i][j] = B[i][j];
                            b1.putInt(B1[i][j]);
                        }
                    }
                    byte[] A= a1.array();
                    byte[] B= b1.array();
                    salida.write(A);
                    Thread.sleep(100);
                    salida.write(B);
                }else if (nodo == 2){ // Nodo 2
                    ByteBuffer a1 = ByteBuffer.allocate(N*N*4);
                    ByteBuffer b2 = ByteBuffer.allocate(N*N*4);
                    for(int i = 0; i < (N/2); i++){
                        for(int j = 0; j < N; j++){
                            A1[i][j] = A[i][j];
                            a1.putInt(A1[i][j]);
                            B2[i][j] = B[i+(N/2)][j];
                            b2.putInt(B2[i][j]);
                        }
                    }
                    byte[] A= a1.array();
                    byte[] B= b2.array();
                    salida.write(A);
                    Thread.sleep(100);
                    salida.write(B);
                }else if (nodo == 3){ // Nodo 3
                    ByteBuffer a2 = ByteBuffer.allocate(N*N*4);
                    ByteBuffer b1 = ByteBuffer.allocate(N*N*4);
                    for(int i = 0; i < (N/2); i++){
                        for(int j = 0; j < N; j++){
                            A2[i][j] = A[i+(N/2)][j];
                            a2.putInt(A2[i][j]);
                            B1[i][j] = B[i][j];
                            b1.putInt(B1[i][j]);
                        }
                    }
                    byte[] A= a2.array();
                    byte[] B= b1.array();
                    salida.write(A);
                    Thread.sleep(100);
                    salida.write(B);
                }else if (nodo == 4){ // Nodo 4
                    ByteBuffer a2 = ByteBuffer.allocate(N*N*4);
                    ByteBuffer b2 = ByteBuffer.allocate(N*N*4);
                    for(int i = 0; i < (N/2); i++){
                        for(int j = 0; j < N; j++){
                            A2[i][j] = A[i+(N/2)][j];
                            a2.putInt(A2[i][j]);
                            B2[i][j] = B[i+(N/2)][j];
                            b2.putInt(B2[i][j]);
                        }
                    }
                    byte[] A= a2.array();
                    byte[] B= b2.array();
                    salida.write(A);
                    Thread.sleep(100);
                    salida.write(B);
                }



                //<<<<<<<<<<<<<<<<<<<<<<Para la  Matrix C dependiendo de que nodo venga
                salida.flush();
                
                int x;
                byte[] c1 = new byte[N*N*2];
                read(entrada,c1,0,N*N*2);
                ByteBuffer c = ByteBuffer.wrap(c1);
                if (nodo == 1) { // Nodo 1
                    for (int i = 0; i < (N / 2); i++) {
                        for (int j = 0; j < N/2; j++) {
                            synchronized(lock){
                                C[i][j]=c.getInt();
                            }

                        }
                    }

                }else if (nodo == 2){ // Nodo 2
                    for(int i = 0; i < (N/2); i++){
                        for(int j = 0; j < N/2; j++) {
                            synchronized(lock){
                                C[i][j+(N/2)]=c.getInt();
                            }
                        }
                    }
                }else if (nodo == 3){ // Nodo 3
                    for(int i = 0; i < (N/2); i++){
                        for(int j = 0; j < N/2; j++){
                            synchronized(lock){
                                C[i+(N/2)][j]=c.getInt();
                            }
                        }
                    }
                }else if (nodo == 4){ // Nodo 4
                    for(int i = 0; i < (N/2); i++){
                        for(int j = 0; j < N/2; j++){
                            synchronized(lock){
                                C[i+(N/2)][j+(N/2)]=c.getInt();
                            }
                        }
                    }
                }
                System.out.println(">>>>> Matriz C"+nodo+" recibida");

                entrada.close();
                salida.close();
                conexion.close();

            } catch (IOException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) throws Exception{
        if (args.length != 1) {
            System.err.println("Uso:");
            System.err.println("java PI <nodo>");
            System.exit(0);
        }

        int nodo = Integer.valueOf(args[0]);

        if (nodo == 0){

            // Inicializamos matrices A B C
            for (int i = 0; i < N; i++){
                for (int j = 0; j < N; j++){
                    A[i][j] = 2*i-j;
                    B[i][j] = 2*i+j;
                    C[i][j] = 0;
                }
            }

            //IMprimimos si N == 4
            if(N==4){
                System.out.println(">>>>>>>>> Matriz A: <<<<<<<<");
                for (int i = 0; i< N; i++){
                    for (int j = 0; j < N; j++){
                        System.out.print(A[i][j] + " ");
                    }
                    System.out.println(" ");
                }

                System.out.println(">>>>>>>>> Matriz B: <<<<<<<<");
                for (int i = 0; i< N; i++){
                    for (int j = 0; j < N; j++){
                        System.out.print(B[i][j] + " ");
                    }
                    System.out.println(" ");
                }
            }

            // Transponemos matriz B
            for (int i = 0; i < N; i++) {
                for (int j = 0; j < i; j++) {
                    int aux = B[i][j];
                    B[i][j] = B[j][i];
                    B[j][i] = aux;
                }
            }

            ServerSocket servidor = new ServerSocket(50000);
            Worker[] w = new Worker[4];

            System.out.println(".....Escuchando.....");

            int i = 0;
            while(i < 4){
                Socket conexion;
                conexion = servidor.accept();
                w[i] = new Worker(conexion);
                w[i].start();

                i++;
            }

            i = 0;

            while(i < 4){
                w[i].join();
                i++;
            }
            //System.out.println("....Sin conexiones... ");

            // Calcular el checksum de C.
            long checksum = 0;
            for( i = 0; i < N; i++){
                for( int j = 0; j < N; j++){
                    checksum += C[i][j];
                }
            }

            // Imprimimos a C si es N=4.
           if (N == 4){
                // Imprimimos C:AxB
                System.out.println(">>>>>>>>> Matriz A: <<<<<<<<");
                for (i = 0; i< N; i++){
                    for (int j = 0; j < N; j++){
                        System.out.print(A[i][j] + " ");
                    }
                    System.out.println(" ");
                }

                System.out.println(">>>>>>>>> Matriz B: <<<<<<<<");
                for (i = 0; i< N; i++){
                    for (int j = 0; j < N; j++){
                        System.out.print(B[i][j] + " ");
                    }
                    System.out.println(" ");
                }

                System.out.println(">>>>>>>>> Matriz C: <<<<<<<<");
                for (i = 0; i< N; i++){
                    for (int j = 0; j < N; j++){
                        System.out.print(C[i][j] + " ");
                    }
                    System.out.println(" ");
                }
            }
            // Mostramos checksum de C.
            System.out.println(" >>>> Checksum es: " + checksum + " ");

        }else{
            //matrices auxiliares
            int[][] auxA=new int[N/2][N];
            int[][] auxB=new int[N/2][N];
            int[][] auxC=new int[N/2][N/2];

            Socket conexion = null;

            for(;;) {
                try {
                    conexion = new Socket("localhost", 50000);
                    System.out.println("Conexión establecida");
                    break;
                } catch (Exception e) {
                    System.out.println("Conexión rechaazada");
                    Thread.sleep(100);
                }
            }

            DataInputStream entrada = new DataInputStream(conexion.getInputStream());
            DataOutputStream salida = new DataOutputStream(conexion.getOutputStream());

            salida.writeInt(nodo);

            //Recibimos matrices A y B
            byte[] a_1 = new byte[N*N*4];
            read(entrada,a_1,0,N*N*4);
            ByteBuffer a = ByteBuffer.wrap(a_1);
            for (int i = 0; i < (N/2); i++){
                for(int j=0;j<N;j++){
                    auxA[i][j]=a.getInt();
                }
            }
            System.out.println(">>>> Matriz Ai recibida");

            byte[] b_1 = new byte[N*N*4];
            read(entrada,b_1,0,N*N*4);
            ByteBuffer b = ByteBuffer.wrap(b_1);
            for (int i = 0; i < (N/2); i++){
                for(int j=0;j<N;j++){
                    auxB[i][j]=b.getInt();
                }
            }
            System.out.println(">>>> Matriz Bi recibida");


            //Calcular 
            System.err.println(">>>>> Calculando la matriz C"+nodo);
            for (int i = 0; i < N/2; i++){
                for (int j = 0; j < N/2; j++){
                    for (int k = 0; k < N; k++)
                        auxC[i][j] += auxA[i][k] * auxB[j][k];
                }
            }
            salida.flush();
            //Enviar resultado
            System.out.println(">>>>>> Enviando Matriz C"+nodo);
            ByteBuffer c = ByteBuffer.allocate(N*N*2);
            for(int i=0;i<N/2;i++){
                for(int j=0;j<N/2;j++){
                    c.putInt(auxC[i][j]);
                }
            }
            byte[] C=c.array();
            salida.write(C);
            // cerramos las transmisiones de entrada, salida y la conexión
                entrada.close();
                salida.close();
                conexion.close();
        }

    }

    // lee del DataInputStream todos los bytes requeridos
    static void read(DataInputStream f,byte[] b,int posicion,int longitud) throws Exception{
        while (longitud > 0){
            int n = f.read(b,posicion,longitud);
            posicion += n;
            longitud -= n;
        }
    }
}
