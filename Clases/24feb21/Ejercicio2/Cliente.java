import java.net.Socket;
import java.io.DataOutputStream;
import java.io.DataInputStream;
import java.nio.ByteBuffer;

class Cliente{
    // lee del DataInputStream todos los bytes requeridos

    static void read(DataInputStream f,byte[] b,int posicion,int longitud) throws Exception{
        while (longitud > 0){
            int n = f.read(b,posicion,longitud);
            posicion += n;
            longitud -= n;
        }
    }

    public static void main(String[] args) throws Exception{
        Socket conexion = new Socket("localhost",50000);

        DataOutputStream salida = new DataOutputStream(conexion.getOutputStream());
        DataInputStream entrada = new DataInputStream(conexion.getInputStream());

        
        int numero = 10000;

        ByteBuffer b = ByteBuffer.allocate(numero*8);


        long inicio = System.currentTimeMillis();

        for(int i = 1; i <= numero; i++){
            b.putDouble((float)i);
        }

        long fin = System.currentTimeMillis();
        double tiempo = (double)(fin - inicio);

        System.out.println("Tiempo para flotantes: " + tiempo + " mili segundos");

        byte[] a = b.array();
        salida.write(a);

        salida.close();
        entrada.close();
        conexion.close();
    }
}