import java.net.Socket;
import java.net.ServerSocket;
import java.io.DataOutputStream;
import java.io.DataInputStream;
import java.nio.ByteBuffer;

class Servidor {
    // lee del DataInputStream todos los bytes requeridos

    static void read(DataInputStream f,byte[] b,int posicion,int longitud) throws Exception
    {
        while (longitud > 0)
        {
            int n = f.read(b,posicion,longitud);
            posicion += n;
            longitud -= n;
        }
    }

    public static void main(String[] args) throws Exception
    {
        ServerSocket servidor = new ServerSocket(50000);

        Socket conexion = servidor.accept();

        DataOutputStream salida = new DataOutputStream(conexion.getOutputStream());
        DataInputStream entrada = new DataInputStream(conexion.getInputStream());

        int numero = 10000;

        byte[] a = new byte[numero*8];
        read(entrada,a,0,numero*8);
        ByteBuffer b = ByteBuffer.wrap(a);

        long inicio = System.currentTimeMillis();

        for (int i = 0; i < numero; i++){
           // System.out.println(b.getDouble());
        }

        long fin = System.currentTimeMillis();
        double tiempo = (double)(fin - inicio);

        System.out.println("Tiempo para flotantes: " + tiempo + " mili segundos");

        salida.close();
        entrada.close();
        conexion.close();
    }
}