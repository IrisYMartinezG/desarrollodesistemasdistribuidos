import java.net.Socket;
import java.io.DataOutputStream;
import java.io.DataInputStream;
import java.nio.ByteBuffer;

class Cliente{
    // lee del DataInputStream todos los bytes requeridos

    static void read(DataInputStream f,byte[] b,int posicion,int longitud) throws Exception{
        while (longitud > 0){
            int n = f.read(b,posicion,longitud);
            posicion += n;
            longitud -= n;
        }
    }

    public static void main(String[] args) throws Exception{
        Socket conexion = new Socket("localhost",50000);

        DataOutputStream salida = new DataOutputStream(conexion.getOutputStream());
        DataInputStream entrada = new DataInputStream(conexion.getInputStream());

        
        //<<<<<<<<<<<<<<<<<<<<  Ejercicio 3 y 4 >>>>>>>>>>>>>>>>>>>>>>>>
        int num = 10000;
        long inicio = System.currentTimeMillis();

        for(int i = 1; i <= num; i++){
            salida.writeDouble((float)i);
        }

        long fin = System.currentTimeMillis();
        double tiempo = (double)(fin - inicio);

        System.out.println("Tiempo para flotantes: " + tiempo + " milisegundos");


        salida.close();
        entrada.close();
        conexion.close();
    }
}