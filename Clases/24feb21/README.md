# **Clase del día 24 de febrero**

Programación de una comunicación de Cliente-Servidor bi-direccional.

### **Envio empaquetado (Bytebuffer) vs Envio individual**


| Ejercicio | Envio |Recibo|
| ------ | ------ |------|
| writedouble |  131 ms |131 ms|
| Bytebuffer | 8 ms | 300 ms|
